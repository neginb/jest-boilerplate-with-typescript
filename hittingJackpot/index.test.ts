/**
testJackpot(["@", "@", "@", "@"]) ➞ true
testJackpot(["abc", "abc", "abc", "abc"]) ➞ true
testJackpot(["SS", "SS", "SS", "SS"]) ➞ true
testJackpot(["&&", "&", "&&&", "&&&&"]) ➞ false
testJackpot(["SS", "SS", "SS", "Ss"]) ➞ false
 */

import {identical} from './index'

it("four should be in this array", () => {
const input= ["@", "@", "@", "@"];
  
    expect(identical(input)).toBe(true);

} )

it("four should not be in this array", () => {
    let array= ["&&", "&", "&&&", "&&&&"];
   
    expect(identical(array)).toBe(false);

} )