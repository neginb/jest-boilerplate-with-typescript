/* howUnlucky(2020) ➞ 2
howUnlucky(2026) ➞ 3
howUnlucky(2016) ➞ 1 */

import {howUnlucky} from '.'

it("There are 2 Friday 13ths in year 2020", () => {
        
        expect(howUnlucky(2020)).toBe(2);
    
} ) 

it("There are 3 Friday 13ths in year 2026", () => {
        
    expect(howUnlucky(2026)).toBe(3);

} ) 

it("There are 1 Friday 13ths in year 2016", () => {
        
    expect(howUnlucky(2016)).toBe(1);

} ) 