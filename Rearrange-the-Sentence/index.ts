export function rearrange(sentence){
    if(!sentence) return '' //här kollar man om sentence är null or undefined eller empty

    const splitSentence = sentence.split(' ')
    let output : String[] = []

    //loop through each word of the sentence
    for(const word of splitSentence){
        let currentWord = ''
        let currentWordPosTag = 0

        //loop through each chsrscter of the word
        for (let i = 0; i < word.length; i++) {
            if (!isNaN(word[i])) currentWordPosTag = word[i] - 1 //grab the number 
            else currentWord += word[i] //build the current word without the number
        }
        output[currentWordPosTag] = currentWord
    }
    return output.join(' ').trim() //we trim in case the input was '1,2,4,5,3'
}