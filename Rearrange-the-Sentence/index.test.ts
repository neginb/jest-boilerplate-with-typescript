import { rearrange } from ".";

test("Should rearrange the words appropriately, and remove the digits from the sentence", () => {
    const [input, expected] = ['is2 Thi1s T4est 3a', 'This is a Test']

    const actual = rearrange(input)
    expect(actual).toBe(expected)
})