/* charCount("a", "edabit") ➞ 1
charCount("c", "Chamber of secrets") ➞ 1
charCount("B", "boxes are fun") ➞ 0
charCount("b", "big fat bubble") ➞ 4
charCount("e", "javascript is good") ➞ 0
charCount("!", "!easy!") ➞ 2
charCount("wow", "the universe is wow") ➞ error */

import { countCharacter } from "."


test("should have 1 same character", () => {

    //Arrange
    const inputEtt = "a"
    const inputTvå = "edabit"
    const expected = 1

    //Act
    const actual = countCharacter(inputEtt, inputTvå)

    //Assert
    expect(actual).toBe(expected)

})


test("should have 1 same character", () => {

    //Arrange
    const inputEtt = "c"
    const inputTvå = "Chamber of secretsedabit"
    const expected = 1

    //Act
    const actual = countCharacter(inputEtt, inputTvå)

    //Assert
    expect(actual).toBe(expected)

})

test("should have 0 same character", () => {

    //Arrange
    const inputEtt = "B"
    const inputTvå = "boxes are fun"
    const expected = 0

    //Act
    const actual = countCharacter(inputEtt, inputTvå)

    //Assert
    expect(actual).toBe(expected)

})

test("should have 4 same character", () => {

    //Arrange
    const inputEtt = "b"
    const inputTvå = "big fat bubble"
    const expected = 4

    //Act
    const actual = countCharacter(inputEtt, inputTvå)

    //Assert
    expect(actual).toBe(expected)

})


test("should have 2 same character", () => {

    //Arrange
    const inputEtt = "!"
    const inputTvå = "!easy!"
    const expected = 2

    //Act
    const actual = countCharacter(inputEtt, inputTvå)

    //Assert
    expect(actual).toBe(expected)

})

test("should throw a error message", () => {

    //Arrange
    const inputEtt = "wow"
    const inputTvå = "the universe is wow"
    const expected = "error"

    //Act
    const actual = countCharacter(inputEtt, inputTvå)

    //Assert
    expect(actual).toBe(expected)

})
