/* isValidIP("1.2.3.4") ➞ true
isValidIP("1.2.3") ➞ false
isValidIP("1.2.3.4.5") ➞ false
isValidIP("123.45.67.89") ➞ true
isValidIP("123.456.78.90") ➞ false
isValidIP("123.045.067.089") ➞ false */

import {isValidIP} from '.'

it("the IPv4 is valid", () => {
    const str = "1.2.3.4"
        
        expect(isValidIP(str)).toBe(true);
    
} ) 

it("the IPv4 is not valid", () => {
    const str = "1.2.3"
        
        expect(isValidIP(str)).toBe(false);
    
} ) 

it("the IPv4 is not valid", () => {
    const str = "1.2.3.4.5"
        
        expect(isValidIP(str)).toBe(false);
    
} ) 

it("the IPv4 is valid", () => {
    const str = "123.45.67.89"
        
        expect(isValidIP(str)).toBe(true);
    
} ) 

it("the IPv4 is not valid", () => {
    const str = "123.456.78.90"
        
        expect(isValidIP(str)).toBe(false);
    
} ) 

it("the IPv4 is not valid", () => {
    const str = "123.045.067.089"
        
        expect(isValidIP(str)).toBe(false);
    
} ) 

it("the IPv4 is not valid", () => {
    const str = "123.112.167.0"
        
        expect(isValidIP(str)).toBe(false);
    
} ) 