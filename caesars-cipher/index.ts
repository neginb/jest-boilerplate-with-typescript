export function ceasarCipher(input,cipher){

    //check input types
    if(typeof input !== 'string')
        throw new Error("Input must be a string");
    if(isNaN(cipher))
        throw new Error("Cipher must be a number");

        //Instantiate empty string
        let output = "";

        //convert each char c in the input according to the cipher
        for(let c of input)
            output += convertChar(c, cipher);
        return output;
}

function convertChar(c, cipher){
    //
    //
    //
    let code = c.charCodeAt(0);
    //
    //
    //
    //
    //
    //
    //
    //
    if((code >= 65 && code <= 90) || (code >=97 && code <= 122))
        return ((code%32 + cipher < 26) ?
            String.fromCharCode(code - (26 - cipher)) :
            String.fromCharCode(code + cipher));
    return c;
}
console.log(ceasarCipher("", 1));