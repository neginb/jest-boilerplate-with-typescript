import shuffleCount from ".";

    it("should throw an error for uneven numbers", () => {
        expect(shuffleCount(3)).toThrow("The number is not even.")
    })

    it("should throw an error for negative numbers", () => {
        expect(shuffleCount(-8)).toThrow("The number is negative.")
    })

    it("should return 3", () => {
        expect(shuffleCount(8)).toBe(3)
    })

    it("should return 12", () => {
        expect(shuffleCount(14)).toBe(12)
    })

    it("should return 8", () => {
        expect(shuffleCount(52)).toBe(8)
    })

