export default function shuffleCount(numberOfCards: number): number {
    let deck: number[] = []
    let count:number = 0

    if(numberOfCards % 2 !== 0) throw new Error("The number is not even.")
    if(Math.sign(numberOfCards) === -1) throw new Error("The number is negative.")

    for(let i = 1; i <= numberOfCards; i++) { deck.push(i)}
    let newDeck: number[] = [...deck]

    do {
        let tmpDeck:number[] = []
        let firstHalf:number[] = newDeck.slice(0, (newDeck.length / 2))
        let secondHalf:number[] = newDeck.slice(newDeck.length / 2)
        for (let i = 0; i < deck.length / 2; i++) {tmpDeck.push(firstHalf[i], secondHalf[i])}
        count++
        newDeck = [...tmpDeck]
    } while (newDeck.toString() !== deck.toString());

    return count
}