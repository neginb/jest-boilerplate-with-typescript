export function anagrams(word, arr){
    //check if first parameter is empty / undefined / null
    if(!word){ throw new Error("First parameter is emepty")}

    //check if the array is empty,ie. length is 0
    if(arr.length === 0){ throw new Error("Second parameter is empty")}

    //array containing the anagrams
    let matches: string[] = []

    // check each word of the array against the original word
    for(const item of arr){
        if(isAnagram(word, item)){
            matches.push(item)
        }
    }
    return matches
}

function isAnagram(wordA, wordB){
    //check if the words are anagrams

    //if they don't have the same length. they can't be anagrams
    if(wordA.length !== wordB.length){
        return false;
    }

    //split and sort the words to be compared
    if(splitAndSort(wordA) === splitAndSort(wordB)){
        //words are anagrams
        return true
    }

    //words are not anagrams
    return false
}

function splitAndSort(word){
    //transform the word into an array, then sort the array
    return word.split('').sort().join('')
}
