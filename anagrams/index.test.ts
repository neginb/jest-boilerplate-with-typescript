/* anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']) => ['aabb', 
'bbaa']
anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']) => 
['carer', 'racer']
anagrams('laser', ['lazing', 'lazy', 'lacer']) => []
 */
import {anagrams} from '.'

test("Should return [].", () => {
    const strings = anagrams('laser', ['lazing', 'lazy', 'lacer']);
    expect(strings).toEqual([])
})

test("Should return ['carer', 'racer'].", () => {
    const strings = anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']);
    expect(strings).toEqual(['carer', 'racer'])
})

test("Should return ['aabb', 'bbaa'].", () => {
    const strings = anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']);
    expect(strings).toEqual(['aabb', 'bbaa'])
})