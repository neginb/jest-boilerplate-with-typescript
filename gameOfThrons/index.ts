export function correctTitle(title){
    let output = ""

    //add space after comma
    title = handleCommasWithoutSpaces(title)

    //split the sentce up by spaces
    const titleToArray = title.split(' ')

    //correct the capitalization of each word
    for (const item of titleToArray){
        if(isLowerCaseWord(item)){
            //and,the,of,in
            //append the word completely lower-cased
            output += item.toLowerCase()
        } else {
            //appende the word lower cased, except for the first letter
            output += item.substring(0, 1).toUpperCase() + item.substring(1, item.length).toLowerCase()
        }
        //add a space after the word has been processed
        output += " "
    }

    //trim the final space from the output before adding a final period(ta bort mellanslaget innan att sätta punkt)
    output = output.trim()
    
    //add a period at the end if needed
    if(title[title.length - 1] !== "."){
        output += "."
    }

    return output
}

function handleCommasWithoutSpaces(word){
    //if there is a comma in the word, and the following character is not a space, add a space
    const pos = word.indexOf(",")

    //if pos > 0, then a comma has been found in the word
    //substring:
    //arg 1: the position to start the substring
    //arg 2: the first character to exclude from the substring
    if(pos > 0 && word.substring(pos + 1, pos + 2) !== " "){
        //console.log("it is going in here!")
        return word.substring(0, pos + 1) + " " + word.substring(pos + 1)
    }
    return word
}

function isLowerCaseWord(word){
    word = word.toLowerCase()
    //check if the word matches a compulsory lower case word
    if(word === "and" || word === "the" || word === "of" || word === "in"){
        return true
    }
    return false
}