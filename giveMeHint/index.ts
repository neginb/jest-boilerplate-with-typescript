
//loops gonna runs (length of longest word +1) times. +1 because the first output is just sträckor

export function grantTheHint(str: string) {
    const arr = str.split(" ")
    const currentHint = getHidden(arr)
    const result = [getHidden(arr).join(" ")]
    const longestWord = arr.reduce((a, b) => { return a.length > b.length ? a : b })

    for (let i = 0; i < longestWord.length; i++) {
        for (let [j, word] of arr.entries()) {
            const newWord = currentHint[j].split("")
            newWord[i] = word[i]
            currentHint[j] = newWord.join("")
        }
        result.push(currentHint.join(" "))
    }
    return result
}

function getHidden(arr: any[string]) {
    return arr.map((w: string) => { return "_".repeat(w.length) })
}

