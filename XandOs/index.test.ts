/* XAndO(board = [" | | ", " |X| ", "X| | "]) ➞ [1, 3]
// Board becomes:
// | |
// | X |
// X | |
XAndO(board = ["X|X|O", "O|X| ", "X|O| "]) ➞ [3, 3]
// Board becomes:
// X | X | O
// O | X |
// X | O | */
