
export function print2largest(arr) {

   
    // sort the array
    arr.sort(function(a,b){return a-b}); //sort the array from smallest to largest

    if(arr.length === 0){
        return 0
    }else if(arr.length === 1){
        return arr[0]
    }else if(arr.length === 2 && arr[0] === arr[1]){
        return 0
    }

    return arr[arr.length - 2]
 
}

