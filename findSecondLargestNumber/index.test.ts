/* secondLargest([10, 40, 30, 20, 50]) ➞ 40
secondLargest([25, 143, 89, 13, 105]) ➞ 105
secondLargest([54, 23, 11, 17, 10]) ➞ 23
secondLargest([1, 1]) ➞ 0
secondLargest([1]) ➞ 1
secondLargest([]) ➞ 0 */

import { print2largest } from "."

it("the second largest number in the array is 40", () => {
    const arr= [10, 40, 30, 20, 50];
      
        expect(print2largest(arr)).toBe(40);
    
    } )

it("the second largest number in the array is 23", () => {
    const arr= [54, 11, 23, 17, 10];
        
        expect(print2largest(arr)).toBe(23);
    
    } )
    
it("the second largest number in the array is 105", () => {
const arr= [25, 143, 89, 13, 105];
    
    expect(print2largest(arr)).toBe(105);

} ) 

it("the second largest number in the array is 0", () => {
    const arr= [1, 1];
        
        expect(print2largest(arr)).toBe(0);
    
    } ) 

it("the second largest number in the array is 0", () => {
    const arr= [];
        
        expect(print2largest(arr)).toBe(0);
    
} ) 
        
    

it("the second largest number in the array is 1", () => {
    const arr= [1];
        
        expect(print2largest(arr)).toBe(1);
    
    } ) 

