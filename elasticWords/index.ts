export function elasticize(word)
{
    let output = word; //instantiate the output, base is the word itself
    let wlength = word.length; //store the length of the input word

    // If the length of a word is smaller than 3, return the word itself
    if(wlength < 3)
        return output;

    // Check whether the word is even or odd in length
    if(wlength%2 == 0) //if even length
    {
        const left = getLeft(word); //call function to get the left half
        const right = getRight(word); //call function to get the right half

        return getOutput(left, right); //generate the elasticized output
    }
    else //if odd length
    {
        const left = getLeft(word);
        const right = getRight(word);
        const center = getCenter(word); //call function to get the center character

        return getOutput(left, right, center); //generate the elasticized output
    }
}

// Returns the left part of a word. This should be range (0, wordlength/2) for both even and odd-length
// words, but it definitely should be safer to use floor(lt/2) in this case.
function getLeft(word)
{
    const lt = word.length;
    const left = word.slice(0, lt/2);

    return left;
}

// Returns the right part of a word. For even-length words, it should be range (wordlength/2, wordlength),
// but for odd-length words you have to take in consideration the center/pivot character and exclude it
// from the right part. In this case you do not start from wordlength/2, but (wordlength/2 + 1)
function getRight(word)
{
    const lt = word.length;
    const right = (lt%2 === 0) ? word.slice(lt/2, lt) : word.slice(lt/2 + 1, lt);

    return right;
}

// Returns the center/ pivot character of an odd-length word. The length is not checked in this function
// because it is only called from the main function if the input word is of odd-length.
function getCenter(word)
{
    const lt = word.length;
    const center = word.slice(lt/2, lt/2 + 1);
    return center;
}

// Returns the elasticized output. The default value of 'center' is an empty string in the case the
// function is called from the even-length part of the main function.
function getOutput(left, right, center = "")
{
    let output = ""; //initialize output
    let n = 1; //initialize variable that stores how often the character gets repeated
    
    // repeat each character lc in the left part n times
    for(let lc of left)
    {
        output += lc.repeat(n);
        n++; // you want each next character to be repeated one more time
    }

    // repeat the center/ pivot character n times
    output += center.repeat(n);
    n--; //center should be repeated the most

    // repeat each character rc in the right part n times
    for(let rc of right)
    {
        output += rc.repeat(n);
        n--; // you want each next character to be repeated one less time
    }

    return output;
}